// nav.js
import bus from '../helpers/bus'

const open = () => {
  let navs = Array(...document.querySelectorAll('.js-nav'))
  navs.forEach(nav => {
    nav.classList.add('is-active')
  })
}

const close = () => {
  let navs = Array(...document.querySelectorAll('.js-nav'))
  navs.forEach(nav => {
    nav.classList.remove('is-active')
  })
}

const bind = () => {
  let buttons = Array(...document.querySelectorAll('.js-open-nav'))
  buttons.forEach(button => {
    button.addEventListener('click', e => {
      e.preventDefault();
      bus.emit('nav:open')
    })
  })
}

const listen = () => {
  bus.off('nav:bind', bind)
  bus.off('nav:open', open)
  bus.off('nav:close', close)
  bus.off('keyboard:escape', close)

  bus.on('nav:bind', bind)
  bus.on('nav:open', open)
  bus.on('nav:close', close)
  bus.on('keyboard:escape', close)

  bus.emit('nav:bind')
}

export default listen
