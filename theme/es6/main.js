// Helpers
import bus from './helpers/bus.js'
import keyboard from './helpers/keyboard.js'

import navigation from './components/nav.js'

const bind = () => {

}
const init = () => {
  bind()
  keyboard()
  navigation()
}

bus.on('app:init', init)
bus.on('app:bind', bind)
bus.emit('app:init');
